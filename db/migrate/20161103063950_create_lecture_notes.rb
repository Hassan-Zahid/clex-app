class CreateLectureNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :lecture_notes do |t|
      t.string :title
      t.references :user, foreign_key: true
      t.references :chapter, foreign_key: true
      t.text :html

      t.timestamps
    end
  end
end
