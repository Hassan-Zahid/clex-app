

# https://gist.github.com/oelmekki/c78cfc8ed1bba0da8cee

Rails.application.config.browserify_rails.commandline_options = "-t [ babelify --presets [ es2015 react stage-0 ] --plugins [ transform-regenerator ] ]"

