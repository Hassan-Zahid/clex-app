# With this minimum investment, we get a battle-tested, full-fledged authentication solution. Here are the main endpoints our User auth API will have:
# http://www.developingandrails.com/2015/02/api-authentication-with-devisetokenauth.html

# / POST  Email registration. Accepts email, password, and password_confirmation params.
# / DELETE  Account deletion. This route destroys users identified by their uid and auth_token headers.
# / PUT Account updates. This route updates an existing user's account settings. The default accepted params are password and password_confirmation.
# /sign_in  POST  Email authentication. Accepts email and password as params. Return a JSON representation of the User model on successful login.
# /sign_out DELETE  Use this route to end the user's current session, by invalidating their authentication token.
# /validate_token GET Use this route to validate tokens on return visits to the client. Accepts uid and auth_token as params.
# /password POST  Use this route to send a password reset confirmation email to users that registered by email.
# /password PUT Use this route to change users' passwords.
# /password/edit  GET Verify user by password reset token. This route is the destination URL for password reset confirmation.



Rails.application.routes.draw do


  devise_for :users
  #mount_devise_token_auth_for 'User', at: 'api/auth'


  root 'home#index'


  namespace :api do
    scope :v1 do
      resource :profile, only: [:show]

      resources :subjects
      resources :chapters
      resources :notes
    end
  end

end
